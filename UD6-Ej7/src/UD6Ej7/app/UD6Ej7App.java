package UD6Ej7.app;

import java.util.Scanner;

public class UD6Ej7App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//PIDO POR TECLADO LA CANTIDAD DE EUROS
		Scanner scan = new Scanner(System.in);
		
		//PIDO LA CANTIDAD DE EUROS
		System.out.println("De cuantos euros quieres realizar la conversi�n?");
		int euros = scan.nextInt();
		
		//TIPO DE MONEDA A LA CUAL SE REALIZARA LA CONVERSI�N
		System.out.println("A que moneda quieres hacer la conversi�n? (libras, d�lares o yenes)");
		String moneda = scan.next();
		
		conversioMoneda(euros, moneda);
	}
	
	//M�TODO PARA CONVERTIR
	public static void conversioMoneda(int euros, String moneda) {
		if (moneda.equals("libras")) {
			System.out.println("La cantidad en libras �s: "+(euros*0.86));
		}else if(moneda.equals("d�lares")){
			System.out.println("La cantidad en d�lares �s "+(euros*1.28611));
		}else if(moneda.equals("yenes")) {
			System.out.println("La cantidad en yenes �s "+(euros*129.852));
		}
	}

}
