package UD6Ej10.app;

import java.util.Scanner;

public class UD6Ej10App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Que tama�o tendr� el array?");
		int tama�o = scan.nextInt();
		
		int array[]=new int [tama�o];
		
		int min=0;
		int max=20;
		
		//LLAMADA A LA FUNCI�N DE RELLENAR ARRAY
		array = rellenarArray(array,min,max);
		
		//LLAMADA AL M�TODO DE MOSTRAR ARRAY
		mostrarArray(array);
		
	}
	
	//FUNCI�N DE RELLENAR ARRAY
		public static int[] rellenarArray(int array[],int min, int max) {
		int randoms[]=array;
		for (int i = 0; i < randoms.length; i++) {
			int numero = (int) (Math.random()*max+min);
			if (esPrimo(numero)) {
				randoms[i] = numero;
				randoms[i] = array[i];
			}
			
		}
		return randoms;
		}
	
	//FUNCI�N PARA COMPROBAR SI EL N�MERO ES PRIMO O NO
	public static boolean esPrimo(int num) {
	    boolean primo = true; 
	    for(int i = 2; i < num; i++) {
	        if (num % i == 0) {
	            primo = false;
	            break;
	        }
	    }
	    return primo;
	}
	
	//FUNCI�N PARA MOSTRAR EL ARRAY
	public static void mostrarArray(int num[]) {
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
	}
	
	

}
