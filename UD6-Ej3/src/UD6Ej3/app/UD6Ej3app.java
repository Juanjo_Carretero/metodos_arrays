package UD6Ej3.app;

import java.util.Scanner;

public class UD6Ej3app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Que n�mero quieres comprobar si es primo?");
		int numero = scan.nextInt();
		
		boolean resultado = esPrimo(numero);
		
		System.out.println(resultado);
	}
	
	
	public static boolean esPrimo(int num) {
	    boolean primo = true; 
	    for(int i = 2; i < num; i++) {
	        if (num % i == 0) {
	            primo = false;
	            break;
	        }
	    }
	    return primo;
	}

}
