package UD6Ej4.app;

import java.util.Scanner;

public class UD6Ej4App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		Scanner scan = new Scanner(System.in);
		
		System.out.println("De que n�mero quieres calcular el factorial");
		int numero = scan.nextInt();
		
		int resultado = factorial(numero);
		
		System.out.println("El factorial de "+numero+" �s: " + resultado);
	}
	
	public static int factorial(int num) {
	    int factorial = 1;
	    
	    while (num!=0) {
	    	factorial=factorial*num;
	    	num--;
	    }
	    return factorial;
	}
}
