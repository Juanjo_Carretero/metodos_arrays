package UD6Ej1.app;

import javax.swing.JOptionPane;

public class UD6Ej1App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String seleccion = JOptionPane.showInputDialog("Teclea una figura para poder realizar su area (circulo, triangulo, cuadrado)"); //ENTRAMOS POR TECLADO LA FIGURA QUE QUERAMOS
		
		if (seleccion.equals("circulo")) {
			String radio = JOptionPane.showInputDialog("Indica el radio de tu circulo");
			
			double radiodouble = Double.parseDouble(radio);
			
			double areacirculo = areaCirculo(radiodouble);
			
			JOptionPane.showMessageDialog(null,"El area de tu circulo �s: "+ areacirculo);
		}
		
		else if (seleccion.equals("triangulo")) {
			String base = JOptionPane.showInputDialog("Indica la base de tu triangulo");
			
			String altura = JOptionPane.showInputDialog("Indica la altura de tu triangulo");
			
			double basedouble = Double.parseDouble(base);
			double alturadouble = Double.parseDouble(base);
			
			double areatriangulo = areaTriangulo(basedouble, alturadouble);
			
			JOptionPane.showMessageDialog(null,"El area de tu circulo �s: "+ areatriangulo);
		}
		
		else if (seleccion.equals("cuadrado")) {
			String lado = JOptionPane.showInputDialog("Indica el lado de tu cuadrado");
			
			double ladodouble = Double.parseDouble(lado);
			
			double areacuadrado = areaCuadrado(ladodouble);
			
			JOptionPane.showMessageDialog(null,"El area de tu cuadrado �s: "+ areacuadrado);
		}
		
	}
	
	//M�TODO DE LA AREA DEL CIRCULO
	public static double areaCirculo (double radio){
		double resultado = Math.pow(radio, 2) * 3.14;
		return resultado;
	}
	
	//M�TODO DE LA AREA DEL TRIANGULO
	public static double areaTriangulo(double base, double altura) {
		double resultado = (base * altura) / 2;
		return resultado;
	}
	
	//M�TEODO DE LA AREA DEL CUADRADO
	public static double areaCuadrado(double lado) {
		double resultado = lado * lado;
		return resultado;
	}

}
