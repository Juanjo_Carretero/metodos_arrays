package UD6Ej5.app;

import java.util.Scanner;

public class UD6Ej5App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		Scanner scan = new Scanner(System.in);
		
		//PIDO POR TECLADO EL N�MERO A PASAR A BINARIO
		System.out.println("Que n�mero quieres pasar a binario?");
		int numero = scan.nextInt();
		
		//RELLENO LA VARIABLE RESULTADO
		String resultado = pasarBinario(numero);
		
		//MUESTRO POR PANTALLA EL RESULTADO
		System.out.println("El n�mero " + numero + " pasado a binario �s: " + resultado);
	}
	
	//M�TODO PARA PASAR DE DECIMAL A BINARIO
	public static String pasarBinario(int numero) {
		
		StringBuilder binario = new StringBuilder();
		
		while (numero > 0) {
			short residuo = (short) (numero % 2);
			numero = numero / 2;
			binario.insert(0, String.valueOf(residuo));
		}
		return binario.toString();
	}
}
