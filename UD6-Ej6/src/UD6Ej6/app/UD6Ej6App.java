package UD6Ej6.app;

import java.util.Scanner;

public class UD6Ej6App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACI�N DE VARIABLES
		Scanner scan = new Scanner(System.in);
		int numero;
		
		do {
			System.out.println("De que n�mero quieres contar las cifras?");
			numero = scan.nextInt();
		} while (!(numero >= 0));
		
		int numcifras = contarCifras(numero);
		
		System.out.println("El n�mero de cifras de " + numero + " �s " + numcifras);
		
	}
	
	public static int contarCifras(int numero) {
		int cifras= 0;
		
		while(numero!=0){             
			numero = numero/10;         
			cifras++;          
        }
		
		return cifras;	
	}
}
