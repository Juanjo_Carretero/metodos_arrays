package UD6Ej12;

import java.util.Scanner;

public class UD6Ej12App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		//DECLARO ANTES LA VARIABLE TAMA�O PARA NO TENER PROBLEMAS
		int tama�o;
		int numerofinal;
		
		//PIDO EL TAMA�O DEL ARRAY
		
		System.out.println("Que tama�o tendr� el array?");
		tama�o = scan.nextInt();
		
		
		//PIDO EL TAMA�O DEL ARRAY
		do {
			System.out.println("Con que n�meros deber�n acabar los n�meros generados?");
			numerofinal = scan.nextInt();
		} while (numerofinal<=0);
		
		//DECLARACI�N DE VARIABLES
		int min=1;
		int max=300;
		
		//DEFINO ARRAYS
		int array[]=new int [tama�o];
		int array2[]=new int [tama�o];
		
		//LLAMADA A LA FUNCI�N DE RELLENAR ARRAY
		array = rellenarArray(array,min,max);
		
		//LLAMADA A LA FUNCI�N DE RELLENAR ARRAY2
		array2 = rellenarArray2(array,tama�o,numerofinal);
		
		//LLAMADA AL M�TODO DE MOSTRAR ARRAY
		System.out.println("Array1");
		mostrarArray(array);
		System.out.println();
		System.out.println("Array2");
		mostrarArray(array2);
	}
	
	//FUNCI�N DE RELLENAR ARRAY
	public static int[] rellenarArray(int array[],int min, int max) {
	int randoms[]=array;
	for (int i = 0; i < randoms.length; i++) {
		int numero = (int) (Math.random()*max+min);
		randoms[i] = numero;
		randoms[i] = array[i];
	}
	return randoms;
	}
	
	//FUNCION DE RELLENAR ARRAY2 CON TERMINACION DE UN NUMERO DEFINIDO
	public static int[] rellenarArray2(int array[],int tama�o,int numerofinal) {
	int retorn[]=new int[tama�o];
	for (int i = 0; i < retorn.length; i++) {
		if (array[i]%10==numerofinal%10) {
			retorn[i]=array[i];
		}
	}
	return retorn;
	}
	
	//FUNCI�N DE MOSTRAR ARRAY
	public static void mostrarArray(int num[]) {
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
	}
	

}
