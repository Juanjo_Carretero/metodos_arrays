package UD6Ej11.app;

import java.util.Scanner;

public class UD6Ej11App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		//DEFINICION DE VARIABLES
		System.out.println("Que tama�o tendr�n los arrays?");
		int tama�o = scan.nextInt();
		
		int array1[]=new int [tama�o];
		int array2[]=new int [tama�o];
		int array3[]=new int [tama�o];
		
		int min=0;
		int max=20;
		
		//LLAMADA A LA FUNCI�N DE RELLENAR ARRAY
		array1 = rellenarArray(array1,min,max);
		
		//VUELCO LOS VALORES
		array2 = array1;
		
		//GENERO NUEVOS VALORES ALEATORIOS AL ARRAY 2
		array2 = rellenarArray(array2, min, max);
		
		//GENERO EL NUEVO ARRAY GRACIAS AL METODO
		array3 = multiplicarArray(array1, array2, tama�o);
		
		//LLAMADA AL M�TODO DE MOSTRAR ARRAY
		System.out.println();
		System.out.println("Array1");
		mostrarArray(array1);
		System.out.println();
		System.out.println("Array2");
		mostrarArray(array2);
		System.out.println();
		System.out.println("Array3");
		mostrarArray(array3);
	}
	
	//FUNCI�N DE RELLENAR ARRAY
	public static int[] rellenarArray(int array[],int min, int max) {
	int randoms[]=array;
	for (int i = 0; i < randoms.length; i++) {
		int numero = (int) (Math.random()*max+min);
		randoms[i] = numero;
		randoms[i] = array[i];
	}
	return randoms;
	}
	
	public static int[] multiplicarArray(int array1[],int array2[], int tama�o) {
	int retorn[]=new int[tama�o];
	for (int i = 0; i < retorn.length; i++) {
		int numero = array1[i]*array2[i];
		
		retorn[i] = numero;
	}
	return retorn;
	}
	
	//FUNCI�N DE MOSTRAR ARRAY
	public static void mostrarArray(int num[]) {
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
	}

}
